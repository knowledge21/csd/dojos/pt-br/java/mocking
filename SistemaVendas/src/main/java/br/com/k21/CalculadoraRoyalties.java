package br.com.k21;

import java.util.List;

import br.com.k21.dao.VendaRepository;
import br.com.k21.modelo.Venda;

public class CalculadoraRoyalties {

	private VendaRepository repositorio;

	public CalculadoraRoyalties(VendaRepository itemfalse) {
		repositorio = itemfalse;
	}

	public double calcularRoyalties(int mes, int ano) {

		List<Venda> lista = repositorio.obterVendasPorMesEAno(ano, mes);

		double faturamento = 0;
		double comissao = 0;

		if (lista != null) {
			for (Venda venda : lista) {
				faturamento += venda.getValor();
				comissao += CalculadoraComissao.Calcular(venda.getValor());
			}
		}

		return (faturamento - comissao) * 0.2;
	}
}
