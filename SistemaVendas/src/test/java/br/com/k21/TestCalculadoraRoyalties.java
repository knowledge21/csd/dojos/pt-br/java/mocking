package br.com.k21;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import br.com.k21.dao.VendaRepository;
import br.com.k21.modelo.Venda;

public class TestCalculadoraRoyalties {

	private VendaRepository vendaRepositoryFalsa;
	private List<Venda> lista;

	@Before
	public void montarMeusMocks()
	{
		vendaRepositoryFalsa = Mockito.mock(VendaRepository.class);
		
		lista = new ArrayList<Venda>();
	}
	
	@Test
	public void teste_mes_sem_vendas_retorna_0() {

		int mes = 7;
		int ano = 2019;
		int result = 0;
		
		Mockito.when(vendaRepositoryFalsa.obterVendasPorMesEAno(ano, mes)).thenReturn(lista);
		
		double royaltiesCalculado = new CalculadoraRoyalties(vendaRepositoryFalsa).calcularRoyalties(mes, ano);

		assertEquals(result, royaltiesCalculado,0);
	}
	
	@Test
	public void teste_mes_uma_venda_retorna_1900() {

		int mes = 6;
		int ano = 2019;
		int result = 1900;
		
		lista.add(new Venda(1, 1, 06, 2019, 10000));
		
		Mockito.when(vendaRepositoryFalsa.obterVendasPorMesEAno(ano, mes)).thenReturn(lista);
		
		double royaltiesCalculado = new CalculadoraRoyalties(vendaRepositoryFalsa).calcularRoyalties(mes, ano);

		assertEquals(result, royaltiesCalculado,0);
	}
	
	@Test
	public void teste_mes_duas_vendas_retorna_4720() {

		int mes = 7;
		int ano = 2019;
		int result = 4720;
		
		lista.add(new Venda(1, 1, 07, 2019, 10000));
		lista.add(new Venda(2, 1, 07, 2019, 15000));
		
		Mockito.when(vendaRepositoryFalsa.obterVendasPorMesEAno(ano, mes)).thenReturn(lista);
		
		double royaltiesCalculado = new CalculadoraRoyalties(vendaRepositoryFalsa).calcularRoyalties(mes, ano);

		assertEquals(result, royaltiesCalculado,0);
	}
	
}
